# demo-wfhconf-gopher 🏠

Homepage of Working From Home Conference (WFHConf) on Gopher.

Gopher: <gopher://wfhconf.commons.host/>

Gopher via Web Proxy: <https://gopher.commons.host/gopher://wfhconf.commons.host/>

Made as a demo for the accompanying presentation:

- **Title**: Gaufre: a Gopher client in your web browser
- **Speaker**: Sebastiaan Deckers
- **Event**: WFH Conf 2020
- **Date**: 2020-03-26
- **Recording**: https://www.youtube.com/watch?v=4OphAbBnZSs

## Development

Edit the Gopher page source code.

```shell
code ./_gopher/gopherhole
```

Start a Gopher server, proxy, and client, for local preview.

```shell
npm start

[...]

Listening on http://localhost:3000
Listening on gopher://localhost:70
Listening on http://localhost:7080
```

Open the Gopher browser in a web browser.

```shell
open http://localhost:3000
```

Open the Gopher browser menu and configure the proxy setting.

```
[x] Custom proxy
    http://localhost:7080
```

## See Also

- [Gopherhole](https://gitlab.com/commonshost/gopherhole) - Static Gopher server for local development
- [Gaufre](https://gitlab.com/commonshost/gaufre) - Gopher browser in the Web browser
- [goh](https://gitlab.com/commonshost/goh) - Gopher over HTTP proxy server
